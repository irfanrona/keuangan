<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
	}

	public function index()
	{
		if ($this->session->has_userdata('role')) {
			redirect(base_url('admin'), 'refresh');
		} else {
			$data['title'] = "Finance App";
			$data['headerContent'] = "Login";


			$data['content'] = $this->load->view('auth/login', $data, TRUE);
			$this->load->view('public_layout', $data);
		}
	}

	public function login()
	{
		//get credential
		$user = $this->UserModel->get_username(@$_POST['username']);
		if (!empty($user)) {
			if ($_POST['username'] == $user[0]->username && $_POST['password'] == $user[0]->password) {
				if (!$user[0]->is_active) {
					$this->session->set_flashdata('flash', 'User belum diaktifkan');
				} else {
					//credential match
					$newdata = array(
						'username'  => $user[0]->username,
						'fullname' => $user[0]->fullname,
						'role' => $user[0]->role,
						'unit' => $user[0]->id_unit,
						'id' => $user[0]->id
					);

					$this->session->set_userdata($newdata);
					$this->session->set_flashdata('flash', 'selamat datang ' . $user[0]->fullname);
				}
			} else {
				//credential didnt match
				$this->session->set_flashdata('flash', 'Username atau Password tidak cocok');
			}
		} else {
			// no username
			$this->session->set_flashdata('flash', 'Username tidak ditemukan');
		}
		redirect(base_url(), 'refresh');
		//echo $this->session->flashdata('flash');
	}

	public function register()
	{
		$data['title'] = "Finance App";
		$data['headerContent'] = "Register";

		$data['content'] = $this->load->view('auth/register', $data, TRUE);
		$this->load->view('public_layout', $data);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url(), 'refresh');
	}
}
