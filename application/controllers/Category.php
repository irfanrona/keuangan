<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('TransactionModel');
		$this->load->model('CategoryModel');
		$this->load->model('UnitModel');

		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}
	}

	public function index()
	{
		$data['title'] = "Finance App";
		$data['headerContent'] = "Rencana Kerja Anggaran";
		$data['active'] = "RKA";

		//query
		//$data['query'] = $this->CategoryModel->get_last_ten_entries();
		$data['user_units'] = $this->UnitModel->get_last_ten_entries();
		$data['transactions'] = $this->TransactionModel->get_last_ten_entries();

		//manipulation data
		$dataRaw = $this->CategoryModel->get_last_ten_entries();
		$i = 0;
		foreach ($dataRaw as $value) {
			$totalPengajuan = 0;
			$totalPenyelesaian = 0;
			$unit = $this->UnitModel->get_name($value->id_user_unit);

			$temp[$i] = new StdClass;
			$temp[$i]->id = $value->id;


			$temp[$i]->id_user_unit = $unit[0]->name;


			$temp[$i]->name = $value->name;
			list($date, $time) = explode(" ", $value->created_at);
			$temp[$i]->created_at = $date;
			$temp[$i]->anggaran = $value->anggaran;

			foreach ($data['transactions'] as $trx) {
				if ($value->id == $trx->category_id) {
					if ($trx->type == 'income') {
						$totalPengajuan += $trx->amount;
					} else if ($trx->type == 'expense') {
						$totalPenyelesaian += $trx->amount;
					}
				}
			}

			$temp[$i]->totalPengajuan = $totalPengajuan;
			$temp[$i]->totalPenyelesaian = $totalPenyelesaian;
			$temp[$i]->selisih = $totalPengajuan - $totalPenyelesaian;

			$i++;
		}
		$data['query'] = $temp;


		$data['content'] = $this->load->view('admin/category/main', $data, TRUE);
		$this->load->view('main_layout', $data);
	}

	public function add()
	{
		//query
		$data['query'] = $this->CategoryModel->insert_entry();
		//flashdata


		redirect(base_url('category'), 'refresh');
	}

	public function edit($id)
	{
		$data['title'] = "Finance App";

		$data['headerContent'] = "RKA | Edit";
		$data['active'] = "RKA";
		//query edit
		$data['query'] = $this->CategoryModel->get_id($id);
		list($date, $time) = explode(" ", $data['query'][0]->created_at);
		$data['query'][0]->created_at = $date;
		$data['user_units'] = $this->UnitModel->get_last_ten_entries();

		$data['content'] = $this->load->view('admin/category/edit-category', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function delete($id)
	{
		//query
		$data['query'] = $this->CategoryModel->delete_entry($id);
		//flashdata


		redirect(base_url('category'), 'refresh');
	}

	public function update($id)
	{
		//query
		$data['query'] = $this->CategoryModel->update_entry($id);
		//flashdata

		redirect(base_url('category'), 'refresh');
	}
}
