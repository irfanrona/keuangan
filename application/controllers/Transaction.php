<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('TransactionModel');
		$this->load->model('CategoryModel');
		$this->load->model('UnitModel');

		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}
	}

	public function reportIncome()
	{
		$data['title'] = "Aplikasi Panjar";
		$data['headerContent'] = "Laporan Pengajuan";
		$data['active'] = "Laporan-pengajuan";

		$dataRaw = $this->TransactionModel->get_last_ten_entries();

		$i = 0;
		foreach ($dataRaw as $value) {
			$category = $this->CategoryModel->get_name($value->category_id);
			$unit = $this->UnitModel->get_name($category[0]->id_user_unit);

			$temp[$i] = new StdClass;
			$temp[$i]->id = $value->id;

			$temp[$i]->category_id = $category[0]->name;
			$temp[$i]->unit_id = @$unit[0]->name;
			$temp[$i]->desc = $value->desc;
			$temp[$i]->amount = $value->amount;
			$temp[$i]->type = $value->type;
			$temp[$i]->approved = $value->approved;

			$i++;
		}
		$data['query'] = $temp;

		$data['content'] = $this->load->view('admin/transaction/report-income', $data, TRUE);
		$data['jscript'] = $this->load->view('admin/transaction/js-transaction', NULL, TRUE);
		$this->load->view('main_layout', $data);
	}

	public function reportExpense()
	{
		$data['title'] = "Aplikasi Panjar";
		$data['headerContent'] = "Laporan Penyelesaian";
		$data['active'] = "Laporan-penyelesaian";

		$dataRaw = $this->TransactionModel->get_last_ten_entries();

		$i = 0;
		foreach ($dataRaw as $value) {
			$category = $this->CategoryModel->get_name($value->category_id);
			$unit = $this->UnitModel->get_name($category[0]->id_user_unit);

			$temp[$i] = new StdClass;
			$temp[$i]->id = $value->id;

			$temp[$i]->category_id = $category[0]->name;
			$temp[$i]->unit_id = @$unit[0]->name;
			$temp[$i]->desc = $value->desc;
			$temp[$i]->amount = $value->amount;
			$temp[$i]->type = $value->type;
			$temp[$i]->approved = $value->approved;

			$i++;
		}
		$data['query'] = $temp;

		$data['content'] = $this->load->view('admin/transaction/report-expense', $data, TRUE);
		$data['jscript'] = $this->load->view('admin/transaction/js-transaction', NULL, TRUE);
		$this->load->view('main_layout', $data);
	}

	public function print()
	{
		$data['title'] = "Finance App";
		$data['headerContent'] = "Laporan";
		$data['active'] = "Laporan-print";

		$dataRaw = $this->TransactionModel->get_last_ten_entries();

		$i = 0;
		foreach ($dataRaw as $value) {
			$category = $this->CategoryModel->get_name($value->category_id);
			$unit = $this->UnitModel->get_name($category[0]->id_user_unit);

			$temp[$i] = new StdClass;
			$temp[$i]->id = $value->id;

			$temp[$i]->category_id = $category[0]->name;
			$temp[$i]->unit_id = @$unit[0]->name;
			$temp[$i]->desc = $value->desc;
			$temp[$i]->amount = $value->amount;
			$temp[$i]->type = $value->type;
			$temp[$i]->approved = $value->approved;

			$i++;
		}
		$data['query'] = $temp;
		$this->load->view('components/htmlheader');
		$this->load->view('admin/transaction/print-transaction', $data);
		$this->load->view('components/htmlfooter');
		$this->load->view('admin/transaction/js-transaction');
	}

	public function add()
	{
		$config['upload_path']          = './uploads/file/trx/';
		$config['allowed_types']        = 'doc|docx|pdf';
		$config['file_name']        	= uniqid();

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('userfile')) {
			$data['error'] =  $this->upload->display_errors();
			$_POST['file_name'] = "";
		} else {
			$data['error'] =  $this->upload->data();
			$_POST['file_name'] = $config['file_name'] . $this->upload->data('file_ext');
		}

		//query
		$data['query'] = $this->TransactionModel->insert_entry();
		//flashdata

		$type = $_POST['type'];
		redirect(base_url('transaction/' . $type), 'refresh');
	}

	public function income()
	{
		$data['title'] = "Finance App";
		if ($this->session->userdata('role') == 'admin') {
			$data['categories'] = $this->CategoryModel->get_last_ten_entries();
		} else {
			$unit = $this->session->userdata('unit');
			$data['categories'] = $this->CategoryModel->get_by_unit_id($unit);
		}

		$data['headerContent'] = "Pengajuan Panjar";
		$data['active'] = "Pengajuan Panjar";
		//query
		if ($this->session->userdata('role') == 'admin') {
			$dataRaw = $this->TransactionModel->get_entries('income');
		} else {
			$unit = $this->session->userdata('unit');
			$categories = $this->CategoryModel->get_by_unit_id($unit);
			foreach ($categories as $category) {
				$units[] = $category->id;
			}
			// var_dump($units);
			$dataRaw = $this->TransactionModel->get_entries_by_unit_id('income', $units);
		}

		$i = 0;
		if (count($dataRaw) > 0) {
			foreach ($dataRaw as $value) {
				$category = $this->CategoryModel->get_name($value->category_id);
				$unit = $this->UnitModel->get_name($category[0]->id_user_unit);

				$temp[$i] = new StdClass;
				$temp[$i]->id = $value->id;

				$temp[$i]->category_id = $category[0]->name;
				$temp[$i]->unit_id = @$unit[0]->name;
				$temp[$i]->desc = $value->desc;
				$temp[$i]->amount = $value->amount;
				$temp[$i]->approved = $value->approved;
				$temp[$i]->message = $value->message;

				if ($value->image != null) {
					$temp[$i]->file = base_url('uploads/file/trx/') . $value->image;
				} else {
					$temp[$i]->file = '#';
				}
				$i++;
			}
			$data['query'] = $temp;
		} else {
			$data['query'] = array();
		}

		//print_r($data['query']);
		$data['content'] = $this->load->view('admin/transaction/add-income', $data, TRUE);


		$this->load->view('main_layout', $data);
	}

	public function expense()
	{
		$data['title'] = "Finance App";
		if ($this->session->userdata('role') == 'admin') {
			$data['categories'] = $this->CategoryModel->get_last_ten_entries();
		} else {
			$unit = $this->session->userdata('unit');
			$data['categories'] = $this->CategoryModel->get_by_unit_id($unit);
		}
		$data['headerContent'] = "Penyelesaian Panjar";
		$data['active'] = "Penyelesaian Panjar";

		//query
		if ($this->session->userdata('role') == 'admin') {
			$dataRaw = $this->TransactionModel->get_entries('expense');
		} else {
			$unit = $this->session->userdata('unit');
			$categories = $this->CategoryModel->get_by_unit_id($unit);
			foreach ($categories as $category) {
				$units[] = $category->id;
			}
			// var_dump($units);
			$dataRaw = $this->TransactionModel->get_entries_by_unit_id('expense', $units);
		}
		//var_dump($dataRaw);
		if (count($dataRaw) > 0) {
			$i = 0;
			foreach ($dataRaw as $value) {
				$category = $this->CategoryModel->get_name($value->category_id);
				$unit = $this->UnitModel->get_name($category[0]->id_user_unit);

				$temp[$i] = new StdClass;
				$temp[$i]->id = $value->id;

				$temp[$i]->category_id = $category[0]->name;
				$temp[$i]->unit_id = @$unit[0]->name;
				$temp[$i]->desc = $value->desc;
				$temp[$i]->amount = $value->amount;
				$temp[$i]->approved = $value->approved;
				$temp[$i]->message = $value->message;

				if ($value->image != null) {
					$temp[$i]->file = base_url('uploads/file/trx/') . $value->image;
				} else {
					$temp[$i]->file = '#';
				}
				$i++;
			}
			$data['query'] = $temp;
		} else {
			$data['query'] = array();
		}


		$data['content'] = $this->load->view('admin/transaction/add-expense', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function edit($id)
	{
		$data['title'] = "Finance App";
		$data['categories'] = $this->CategoryModel->get_last_ten_entries();

		$data['headerContent'] = "Transaksi | Edit";
		$data['active'] = "Transaksi";
		//query edit

		$value = $this->TransactionModel->get_id($id);
		$i = 0;
		$temp[$i] = new StdClass;
		$temp[$i]->id = $value[0]->id;

		$temp[$i]->category_id = $value[0]->category_id;
		$temp[$i]->desc = $value[0]->desc;
		$temp[$i]->amount = $value[0]->amount;
		$temp[$i]->approved = $value[0]->approved;
		$temp[$i]->message = $value[0]->message;

		if ($value[0]->image != null) {
			$temp[$i]->file = base_url('uploads/file/trx/') . $value[0]->image;
		} else {
			$temp[$i]->file = '#';
		}
		$temp[$i]->type = $value[0]->type;
		$temp[$i]->created_at = $value[0]->created_at;
		$i++;
		$data['query'] = $temp;

		$data['content'] = $this->load->view('admin/transaction/edit-transaction', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function delete($id)
	{
		//query
		$data['query'] = $this->TransactionModel->delete_entry($id);
		//flashdata

		$previous = "javascript:history.go(-1)";
		if (isset($_SERVER['HTTP_REFERER'])) {
			$previous = $_SERVER['HTTP_REFERER'];
		}
		redirect($previous, 'refresh');
	}

	public function approve($id)
	{
		$data['title'] = "Aplikasi Panjar";
		$data['categories'] = $this->CategoryModel->get_last_ten_entries();

		$data['headerContent'] = "Transaksi | Approve";
		$data['active'] = "Transaksi";
		//query edit
		$data['query'] = $this->TransactionModel->get_id($id);

		$data['content'] = $this->load->view('admin/transaction/edit-transaction-message', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	// public function revoke($id)
	// {
	// 	//query
	// 	$data['query'] = $this->TransactionModel->revoke($id);
	// 	//flashdata

	// 	$previous = "javascript:history.go(-1)";
	// 	if (isset($_SERVER['HTTP_REFERER'])) {
	// 		$previous = $_SERVER['HTTP_REFERER'];
	// 	}
	// 	redirect($previous, 'refresh');
	// }

	public function update($id)
	{
		$config['upload_path']          = './uploads/file/trx/';
		$config['allowed_types']        = 'pdf|doc|docx';
		$config['file_name']        	= uniqid();

		$this->load->library('upload', $config);

		$_POST['file_name'] = $this->upload->data('file_name');

		if (!$this->upload->do_upload('userfile')) {
			$data['error'] =  $this->upload->display_errors();
			$_POST['file_name'] = 'not set';
		} else {
			$data['error'] =  $this->upload->data();
			$_POST['file_name'] = $config['file_name'] . $this->upload->data('file_ext');
		}


		//query
		$data['query'] = $this->TransactionModel->update_entry($id);
		//flashdata

		// redirect(base_url('transaction/') . $_POST['type'], 'refresh');
	}

	public function approval($id)
	{
		//query
		$data['query'] = $this->TransactionModel->approval($id);
		//flashdata

		redirect(base_url('transaction/') . $_POST['type'], 'refresh');
	}
}
