<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Unit extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UnitModel');

		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}
	}

	public function index()
	{
		$data['title'] = "Finance App";
		$data['headerContent'] = "Unit Kerja";
		$data['active'] = "Unit Kerja";

		//query
		$data['query'] = $this->UnitModel->get_last_ten_entries();

		$data['content'] = $this->load->view('admin/unit/main', $data, TRUE);
		$this->load->view('main_layout', $data);
	}

	public function add()
	{
		//query
		$data['query'] = $this->UnitModel->insert_entry();
		//flashdata


		redirect(base_url('unit'), 'refresh');
	}

	public function edit($id)
	{
		$data['title'] = "Finance App";

		$data['headerContent'] = "Unit Kerja | Edit";
		$data['active'] = "Unit Kerja";
		//query edit
		$data['query'] = $this->UnitModel->get_id($id);

		$data['content'] = $this->load->view('admin/unit/edit-unit', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function delete($id)
	{
		//query
		$data['query'] = $this->UnitModel->delete_entry($id);
		//flashdata


		redirect(base_url('unit'), 'refresh');
	}

	public function update($id)
	{
		//query
		$data['query'] = $this->UnitModel->update_entry($id);
		//flashdata

		redirect(base_url('unit'), 'refresh');
	}
}
