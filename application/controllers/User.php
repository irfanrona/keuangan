<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('UnitModel');
	}

	public function index()
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		$data['title'] = "Finance App";
		$data['headerContent'] = "User";
		$data['active'] = "User";

		//query
		$data['query'] = $this->UserModel->get_last_ten_entries();
		$data['units'] = $this->UnitModel->get_last_ten_entries();

		$data['content'] = $this->load->view('admin/user/main', $data, TRUE);
		$this->load->view('main_layout', $data);
	}

	public function add()
	{
		if (isset($_POST['repassword'])) {
			if ($_POST['repassword'] != $_POST['password']) {
				$this->session->set_flashdata('flash', 'Password tidak sama');
				redirect(base_url('auth/register'), 'refresh');
			}
		}
		//query
		$data['query'] = $this->UserModel->insert_entry();
		//flashdata
		$this->session->set_flashdata('flash', 'User berhasil dibuat');
		redirect(base_url('user'), 'refresh');
	}

	public function edit($id)
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		$data['title'] = "Finance App";
		$data['headerContent'] = "User | Edit";
		$data['active'] = "User";

		//query edit
		$data['query'] = $this->UserModel->get_id($id);
		$data['units'] = $this->UnitModel->get_last_ten_entries();

		$data['content'] = $this->load->view('admin/user/edit-user', $data, TRUE);

		$this->load->view('main_layout', $data);
	}

	public function delete($id)
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		//query
		$data['query'] = $this->UserModel->delete_entry($id);
		//flashdata


		redirect(base_url('user'), 'refresh');
	}

	public function update($id)
	{
		if ($this->session->has_userdata('role')) {
			//redirect(base_url('admin'), 'refresh');
		} else {
			redirect(base_url(), 'refresh');
		}

		//query
		$data['query'] = $this->UserModel->update_entry($id);
		//flashdata

		redirect(base_url('user'), 'refresh');
	}
}
