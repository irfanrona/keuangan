<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CategoryModel extends CI_Model
{

	public $id_user_unit = null;
	public $name;
	public $anggaran = 0;
	public $active = 1;
	public $created_at;
	public $updated_at;

	public function get_last_ten_entries()
	{
		$this->db->order_by('id_user_unit', 'DESC');
		$query = $this->db->get_where('categories', array('active' => 1), 100);
		return $query->result();
	}

	public function get_id($id)
	{
		$query = $this->db->get_where('categories', array('id' => $id));
		return $query->result();
	}

	public function get_by_unit_id($unit_id)
	{
		$query = $this->db->get_where('categories', array('id_user_unit' => $unit_id, 'active' => 1), 100);
		return $query->result();
	}

	public function insert_entry()
	{
		$this->id_user_unit    = $_POST['id_user_unit']; // please read the below note
		$this->name    = $_POST['name'];
		$this->anggaran    = $_POST['anggaran'];
		$this->created_at  = $_POST['created_at'];
		$this->updated_at     = date('Y-m-d H:i:s');

		$this->db->insert('categories', $this);
	}

	public function update_entry($id)
	{
		$this->id_user_unit    = $_POST['id_user_unit']; // please read the below note
		$this->name    = $_POST['name'];
		$this->anggaran    = $_POST['anggaran'];
		$this->created_at = $_POST['created_at'];
		$this->updated_at = date('Y-m-d H:i:s');

		$this->db->update('categories', $this, array('id' => $id));
	}

	public function delete_entry($id)
	{
		$this->active = 0;
		$this->db->update('categories', array('active' => $this->active), array('id' => $id));
	}

	public function get_name($id)
	{
		$query = $this->db->get_where('categories', array('id' => $id), 1);
		return $query->result();
	}
}
