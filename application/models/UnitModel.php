<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UnitModel extends CI_Model
{

	public $name;
	public $active = 1;
	public $created_at;
	public $updated_at;

	public function get_last_ten_entries()
	{
		$query = $this->db->get_where('user_units', array('active' => 1), 100);
		return $query->result();
	}

	public function get_id($id)
	{
		$query = $this->db->get_where('user_units', array('id' => $id));
		return $query->result();
	}

	public function insert_entry()
	{
		$this->name    = $_POST['name']; // please read the below note
		$this->created_at  = date('Y-m-d H:i:s');
		$this->updated_at     = date('Y-m-d H:i:s');

		$this->db->insert('user_units', $this);
	}

	public function update_entry($id)
	{
		$this->name = $_POST['name'];
		$this->created_at = $_POST['created_at'];
		$this->updated_at = date('Y-m-d H:i:s');

		$this->db->update('user_units', $this, array('id' => $id));
	}

	public function delete_entry($id)
	{
		$this->active = 0;
		$this->db->update('user_units', array('active' => $this->active), array('id' => $id));
	}

	public function get_name($id)
	{
		$query = $this->db->get_where('user_units', array('id' => $id), 1);
		return $query->result();
	}
}
