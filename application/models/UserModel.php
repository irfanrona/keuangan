<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserModel extends CI_Model
{

	public $username;
	public $email;
	public $password;
	public $id_unit = NULL;
	public $remember_token;
	public $fullname;
	public $is_active;
	public $role;
	public $created_at;
	public $updated_at;

	public function get_last_ten_entries()
	{
		$query = $this->db->get('users', 100);
		return $query->result();
	}

	public function get_id($id)
	{
		$query = $this->db->get_where('users', array('id' => $id));
		return $query->result();
	}

	public function insert_entry()
	{
		$this->username    = $_POST['username'];
		$this->email    = $_POST['email'];
		$this->password    = $_POST['password'];
		$this->id_unit    = $_POST['id_unit'];
		$this->fullname    = $_POST['fullname'];
		$this->role    = $_POST['role'];
		$this->is_active    = $_POST['is_active'];
		$this->created_at  = date('Y-m-d H:i:s');
		$this->updated_at     = date('Y-m-d H:i:s');

		$this->db->insert('users', $this);
	}

	public function update_entry($id)
	{
		$this->username    = $_POST['username'];
		$this->email    = $_POST['email'];
		$this->password    = $_POST['password'];
		$this->id_unit    = $_POST['id_unit'];
		$this->fullname    = $_POST['fullname'];
		$this->role    = $_POST['role'];
		$this->is_active    = $_POST['is_active'];
		$this->created_at = $_POST['created_at'];
		$this->updated_at = date('Y-m-d H:i:s');

		$this->db->update('users', $this, array('id' => $id));
	}

	public function delete_entry($id)
	{
		$this->db->delete('users', array('id' => $id));
	}

	public function get_name($id)
	{
		$query = $this->db->get_where('users', array('id' => $id), 1);
		return $query->result();
	}

	public function get_username($username)
	{
		$query = $this->db->get_where('users', array('username' => $username), 1);
		return $query->result();
	}
}
