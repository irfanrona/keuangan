﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
$previous = "javascript:history.go(-1)";
if (isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">edit</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Edit RKA (Rencana Kerja Anggaran)</h4>
                            <form method="post" action="<?php echo base_url('category/update/') . $query[0]->id; ?>">
                                <input type="hidden" name="created_at" value="<?= $query[0]->created_at; ?>" />
                                <div class="form-group label-floating">
                                    <label class="control-label">Nama RKA</label>
                                    <input class="form-control" type="text" name="name" required="true" value="<?= $query[0]->name; ?>" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Unit Kerja</label>
                                    <select class="selectpicker" name="id_user_unit" data-style="select-with-transition" title="Unit Kerja" data-size="7" required="true">
                                        <option disabled> Pilih Unit Kerja</option>
                                        <?php
                                        foreach ($user_units as $user_unit) {
                                        ?>
                                            <option value="<?= $user_unit->id ?>" <?php if ($query[0]->id_user_unit == $user_unit->id) {
                                                                                        echo 'selected';
                                                                                    } ?>><?= $user_unit->name ?> </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Jumlah Anggaran</label>
                                    <input class="form-control" type="number" name="anggaran" required="true" value="<?= $query[0]->anggaran; ?>" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Tanggal Pengajuan</label>
                                    <input class="form-control" type="date" name="created_at" required="true" value="<?= $query[0]->created_at; ?>" />
                                </div>
                                <button type="submit" class="btn btn-fill btn-rose">Sunting</button>
                                <a href="<?= $previous; ?>">
                                    <button type="submit" class="btn btn-secondary">Cancel</button>
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>