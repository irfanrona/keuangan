﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8">
                    <div class="card card-plain">
                        <div class="card-header card-header-icon" data-background-color="blue">
                            <i class="material-icons">view_list</i>
                        </div>
                        <h4 class="card-title">Tabel RKA (Rencana Kerja Anggaran)</h4>
                        <p class="category">List RKA</p>
                        <div class="card-content table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Unit Kerja</th>
                                    <th>Tanggal Pengajuan</th>
                                    <th>Jumlah Anggaran</th>
                                    <th>Total Pengajuan</th>
                                    <th>Total Penyelesaian</th>
                                    <th>Selisih Pengajuan</th>
                                    <th>Aksi</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($query as $row) {
                                        $i++;
                                        $url = base_url('category/delete/') . $row->id;
                                    ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= $row->name; ?></td>
                                            <td>
                                                <?php
                                                if ($row->id_user_unit == NULL) {
                                                    echo "Belum Tersedia";
                                                } else {
                                                    echo $row->id_user_unit;
                                                }
                                                ?>
                                            </td>
                                            <td><?= $row->created_at; ?></td>
                                            <td class="text-right"><?php if ($row->anggaran > 0) {
                                                                        echo rupiah($row->anggaran);
                                                                    } else {
                                                                        echo "Belum Tersedia";
                                                                    } ?></td>
                                            <td class="text-right"><?= rupiah($row->totalPengajuan); ?></td>
                                            <td class="text-right"><?= rupiah($row->totalPenyelesaian); ?></td>
                                            <td class="text-right"><?= rupiah($row->selisih); ?></td>
                                            <td class="td-actions">
                                                <a href="<?= base_url('category/edit/') . $row->id; ?>">
                                                    <button type="button" rel="tooltip" class="btn btn-success">
                                                        <i class="material-icons">edit</i>
                                                    </button>
                                                </a>
                                                <button type="button" rel="tooltip" class="btn btn-danger">
                                                    <i class="material-icons" onclick="demo.showSwal('warning-message-and-cancel','<?= $url ?>')">close</i>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class=" col-md-2">
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">add</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Tambah RKA (Rencana Kerja Anggaran)</h4>
                            <form method="post" action="<?php echo base_url() ?>category/add">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nama</label>
                                    <input class="form-control" type="text" name="name" required="true" />
                                </div>
                                <div class="form-group label-floating">
                                    <select class="selectpicker" name="id_user_unit" data-style="select-with-transition" title="Unit Kerja" data-size="7" required="true">
                                        <option disabled> Pilih Unit Kerja</option>
                                        <?php
                                        foreach ($user_units as $user_unit) {
                                        ?>
                                            <option value="<?= $user_unit->id ?>"><?= $user_unit->name ?> </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Jumlah Anggaran</label>
                                    <input class="form-control" type="number" name="anggaran" required="true" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Tanggal Pengajuan</label>
                                    <input class="form-control" type="date" name="created_at" required="true" value="<?= date('Y-m-d'); ?>" />
                                </div>
                                <button type="submit" class="btn btn-fill btn-rose">Tambahkan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>