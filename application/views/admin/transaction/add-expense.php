﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-9">
                    <div class="card card-plain">
                        <div class="card-header card-header-icon" data-background-color="blue">
                            <i class="material-icons">assignment</i>
                        </div>
                        <h4 class="card-title">Tabel Penyelesaian Panjar</h4>
                        <p class="category">List seluruh Penyelesaian Panjar</p>
                        <div class="card-content table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th>No</th>
                                    <th>RKA</th>
                                    <th>Unit Kerja</th>
                                    <th>Deskripsi</th>
                                    <th>Jumlah</th>
                                    <th>Status</th>
                                    <th>File</th>
                                    <th>Aksi</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    if (count($query) == 0) { ?>
                                        <tr>
                                            <td colspan="7" class="text-center">Data is Empty</td>
                                        </tr>
                                    <?php }
                                    foreach ($query as $row) {
                                        $i++;
                                        $url = base_url('transaction/delete/') . $row->id;
                                        $url_approve = base_url('transaction/approve/') . $row->id;
                                        $url_revoke = base_url('transaction/revoke/') . $row->id;

                                        if ($row->message != null) {
                                            $message = $row->message;
                                        } else {
                                            $message = "Tidak Tersedia";
                                        }
                                    ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= $row->category_id; ?></td>
                                            <td><?= $row->unit_id; ?></td>
                                            <td><?= $row->desc; ?></td>
                                            <td class="text-right"><?= rupiah($row->amount); ?></td>
                                            <td><?php if ($row->approved == 1) {
                                                    echo "Approved";
                                                } else {
                                                    echo "Pending";
                                                }; ?>
                                                <button type="button" rel="tooltip" class="btn btn-just-icon btn-simple btn-google" data-original-title="Lihat Keterangan" onclick="demo.showSwal('status-approve','#','<?= $message ?>')">
                                                    <i class="material-icons tiny">chat_bubble_outline</i>
                                                </button>
                                            </td>
                                            <td>
                                                <div style="max-width: 100px;">

                                                    <a href="<?= $row->file; ?>" <?php if ($row->file != '#') { ?> target="_blank" <?php } ?>>
                                                        Download <?php if ($row->file == '#') { ?> Not Available <?php } ?>
                                                    </a>

                                                </div>
                                            </td>
                                            <td class="td-actions">
                                                <?php
                                                if ($this->session->userdata('role') == 'admin') {
                                                    if ($row->approved == 0) { ?>
                                                        <a href="<?= base_url('transaction/approve/') . $row->id; ?>">
                                                            <button type="button" rel="tooltip" class="btn btn-info">
                                                                Approve
                                                            </button>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a href="<?= base_url('transaction/approve/') . $row->id; ?>">
                                                            <button type="button" rel="tooltip" class="btn btn-warning">
                                                                Revoke
                                                            </button>
                                                        </a>
                                                <?php }
                                                } ?>
                                                <a href="<?= base_url('transaction/edit/') . $row->id; ?>">
                                                    <button type="button" rel="tooltip" class="btn btn-success">
                                                        <i class="material-icons">edit</i>
                                                    </button>
                                                </a>
                                                <button type="button" rel="tooltip" class="btn btn-danger">
                                                    <i class="material-icons" onclick="demo.showSwal('warning-message-and-cancel','<?= $url ?>')">close</i>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">add</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Tambah Penyelesaian Panjar</h4>
                            <form method="post" action="<?php echo base_url() ?>transaction/add" enctype="multipart/form-data">
                                <input type="hidden" name="type" value="expense" />
                                <input type="hidden" name="message" />
                                <div class="form-group label-floating">
                                    <select class="selectpicker" name="category_id" data-style="select-with-transition" title="RKA" data-size="7" required="true">
                                        <option disabled> Pilih RKA</option>
                                        <?php
                                        foreach ($categories as $row) {
                                        ?>
                                            <option value="<?= $row->id ?>"><?= $row->name ?> </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Deskripsi</label>
                                    <input class="form-control" type="text" name="desc" required="true" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Jumlah</label>
                                    <input class="form-control" type="number" name="amount" required="true" />
                                </div>
                                <!-- Upload file image disabled, change to PDF -->
                                <!-- <label class="control-label">Foto</label>
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="<?= base_url('assets/img/image_placeholder.jpg'); ?>" alt="...">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div>
                                        <span class="btn btn-info btn-round btn-file">
                                            <span class="fileinput-new">Pilih foto</span>
                                            <span class="fileinput-exists">Ganti</span>
                                            <input type="file" name="userfile">
                                        </span>
                                        <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
                                    </div>
                                </div> -->
                                <label class="control-label">Upload File PDF</label>
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="<?= base_url('assets/img/image_placeholder.jpg'); ?>" alt="...">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div>
                                        <span class="btn btn-info btn-round btn-file">
                                            <span class="fileinput-new">Pilih file PDF</span>
                                            <span class="fileinput-exists">Ganti</span>
                                            <input type="file" name="userfile">
                                        </span>
                                        <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-fill btn-rose">Tambahkan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>