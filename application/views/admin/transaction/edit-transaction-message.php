﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
$previous = "javascript:history.go(-1)";
if (isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">edit</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Edit Transaksi</h4>
                            <form method="post" action="<?php echo base_url('transaction/approval/') . $query[0]->id; ?>">
                                <input type="hidden" name="created_at" value="<?= $query[0]->created_at; ?>" />
                                <input type="hidden" name="type" value="<?= $query[0]->type; ?>" />
                                <input type="hidden" name="category_id" value="<?= $query[0]->category_id; ?>" />
                                <input type="hidden" name="desc" value="<?= $query[0]->desc; ?>" />
                                <input type="hidden" name="amount" value="<?= $query[0]->amount; ?>; ?>" />

                                <?php if ($query[0]->message != null) { ?>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pesan Persetujuan Sebelumnya</label>
                                        <input class="form-control" disabled type="text" name="not_message" value="<?= $query[0]->message; ?>" />
                                    </div>
                                <?php } ?>

                                <div class="form-group label-floating">
                                    <label class="control-label">Status Penyetujuan</label>
                                    <select class="selectpicker" name="approved" data-style="select-with-transition" title="Status Penyetujuan" data-size="7" required="true" <?php if ($this->session->userdata('role') != 'admin') {
                                                                                                                                                                                    echo 'disabled';
                                                                                                                                                                                } ?>>
                                        <option disabled> Pilih Status</option>
                                        <option value="0" <?php if ($query[0]->approved == 0) {
                                                                echo 'selected';
                                                            } ?>>Pending</option>
                                        <option value="1" <?php if ($query[0]->approved == 1) {
                                                                echo 'selected';
                                                            } ?>>Approved</option>
                                    </select>
                                </div>

                                <div class="form-group label-floating">
                                    <label class="control-label">Pesan Persetujuan</label>
                                    <input class="form-control" type="text" name="message" required="true" />
                                </div>

                                <button type="submit" class="btn btn-fill btn-rose">Sunting</button>
                                <a href="<?= $previous; ?>" class="btn btn-secondary">
                                    Cancel
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>