﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
$previous = "javascript:history.go(-1)";
if (isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">edit</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Edit Transaksi</h4>
                            <form method="post" action="<?php echo base_url('transaction/update/') . $query[0]->id; ?>" enctype="multipart/form-data">
                                <input type="hidden" name="created_at" value="<?= $query[0]->created_at; ?>" />
                                <div class="form-group label-floating">
                                    <label class="control-label">Jenis</label>
                                    <select class="selectpicker" name="type" data-style="select-with-transition" title="Pengajuan Panjar / Penyelesaian Panjar" data-size="7" required="true">
                                        <option disabled> Pilih Jenis</option>
                                        <option value="income" <?php if ($query[0]->type == 'income') {
                                                                    echo 'selected';
                                                                } ?>>Pengajuan Panjar</option>
                                        <option value="expense" <?php if ($query[0]->type == 'expense') {
                                                                    echo 'selected';
                                                                } ?>>Penyelesaian Panjar</option>
                                    </select>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Rencana Kerja Anggaran</label>
                                    <select class="selectpicker" name="category_id" data-style="select-with-transition" title="RKA" data-size="7" required="true">
                                        <option disabled> Pilih RKA</option>
                                        <?php
                                        foreach ($categories as $row) {
                                        ?>
                                            <option value="<?= $row->id ?>" <?php if ($query[0]->category_id == $row->id) {
                                                                                echo 'selected';
                                                                            } ?>><?= $row->name ?> </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Deskripsi</label>
                                    <input class="form-control" type="text" name="desc" required="true" value="<?= $query[0]->desc; ?>" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Jumlah</label>
                                    <input class="form-control" type="number" name="amount" required="true" value="<?= $query[0]->amount; ?>" />
                                </div>
                                <!-- Upload Image disabled, change to PDF -->
                                <!-- <label class="control-label">Foto</label>
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="<?= $query[0]->image; ?>" alt="...">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div>
                                        <span class="btn btn-info btn-round btn-file">
                                            <span class="fileinput-new">Pilih foto</span>
                                            <span class="fileinput-exists">Ganti</span>
                                            <input type="file" name="userfile">
                                        </span>
                                        <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
                                    </div>
                                </div> -->
                                <label class="control-label">File PDF</label>
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <?php
                                        if ($query[0]->file != '#') { ?>
                                            <a class="btn btn-simple" href="<?= $query[0]->file; ?>" target="_blank">
                                                <span class="text-success"><i class="fa fa-file-pdf-o"></i> File PDF</span>
                                            </a>
                                        <?php } else { ?>
                                            <button class="btn btn-simple disabled">

                                                <span class="text-warning"><i class="fa fa-file-pdf-o"></i> File empty</span>

                                            </button>
                                        <?php } ?>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div>
                                        <span class="btn btn-info btn-round btn-file">
                                            <span class="fileinput-new">Pilih PDF</span>
                                            <span class="fileinput-exists">Ganti</span>
                                            <input type="file" name="userfile">
                                        </span>
                                        <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
                                    </div>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Status Penyetujuan</label>
                                    <select class="selectpicker" name="approved" data-style="select-with-transition" title="Status Penyetujuan" data-size="7" required="true" <?php if ($this->session->userdata('role') != 'admin') {
                                                                                                                                                                                    echo 'disabled';
                                                                                                                                                                                } ?>>
                                        <option disabled> Pilih Status</option>
                                        <option value="0" <?php if ($query[0]->approved == 0) {
                                                                echo 'selected';
                                                            } ?>>Pending</option>
                                        <option value="1" <?php if ($query[0]->approved == 1) {
                                                                echo 'selected';
                                                            } ?>>Approved</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-fill btn-rose">Sunting</button>
                                <a href="<?= $previous; ?>" class="btn btn-secondary">
                                    Cancel
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>