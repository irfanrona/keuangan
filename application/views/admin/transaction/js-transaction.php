<script type="text/javascript">
    $(document).ready(function() {

        var table1 = $('#datatables-pengajuan').DataTable({
            initComplete: function() {
                this.api().columns([2]).every(function() {
                    var column = this;
                    var select = $('<select class="btn-xs"><option value="">Pilih Semua</option></select>')
                        .appendTo($(column.header()))
                        .on('change', function() {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });
            },
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Cari Data",
            }
        });

        var table2 = $('#datatables-penyelesaian').DataTable({
            initComplete: function() {
                this.api().columns([2]).every(function() {
                    var column = this;
                    var select = $('<select class="btn-xs"><option value="">Pilih Semua</option></select>')
                        .appendTo($(column.header()))
                        .on('change', function() {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });
            },
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Cari Data",
            }
        });

        // Edit record
        table1.on('click', '.edit', function() {
            $tr = $(this).closest('tr');

            var data = table1.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table1.on('click', '.remove', function(e) {
            $tr = $(this).closest('tr');
            table1.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table1.on('click', '.like', function() {
            alert('You clicked on Like button');
        });

        $('.card .material-datatables label').addClass('form-group');

        // Edit record
        table2.on('click', '.edit', function() {
            $tr = $(this).closest('tr');

            var data = table2.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table2.on('click', '.remove', function(e) {
            $tr = $(this).closest('tr');
            table2.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table2.on('click', '.like', function() {
            alert('You clicked on Like button');
        });

        $('.card .material-datatables label').addClass('form-group');
    });
</script>