<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="content">
    <div class="container-fluid">
        <!-- Dihapus tampilan grafik tahunan -->
        <!-- <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="blue">
                        <i class="material-icons">insert_chart</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Laporan Keuangan
                            <small>- Per satu tahun</small>
                        </h4>
                    </div>
                    <div id="multipleBarsChart" class="ct-chart"></div>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="blue">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Laporan Transaksi Pengajuan</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables">
                            <table id="datatables-pengajuan" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>RKA</th>
                                        <th>Unit Kerja </th>
                                        <th>Deskripsi</th>
                                        <th>Jumlah</th>
                                        <th>Status</th>
                                        <th class="disabled-sorting text-right">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($query as $row) {
                                        if ($row->type == 'income') {
                                            $i++;
                                            $url = base_url('transaction/delete/') . $row->id;
                                    ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td><?= $row->category_id; ?></td>
                                                <td><?= $row->unit_id; ?></td>
                                                <td><?= $row->desc; ?></td>
                                                <td class="text-right"><?= rupiah($row->amount); ?></td>
                                                <td><?php if ($row->approved == 1) {
                                                        echo "Approved";
                                                    } else {
                                                        echo "Pending";
                                                    }; ?>
                                                </td>
                                                <td class="td-actions">
                                                    <a href="<?= base_url('transaction/edit/') . $row->id; ?>">
                                                        <button type="button" rel="tooltip" class="btn btn-success">
                                                            <i class="material-icons">edit</i>
                                                        </button>
                                                    </a>
                                                    <button type="button" rel="tooltip" class="btn btn-danger">
                                                        <i class="material-icons" onclick="demo.showSwal('warning-message-and-cancel','<?= $url ?>')">close</i>
                                                    </button>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
    </div>
</div>