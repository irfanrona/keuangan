﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
$previous = "javascript:history.go(-1)";
if (isset($_SERVER['HTTP_REFERER'])) {
    $previous = $_SERVER['HTTP_REFERER'];
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">edit</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Edit User</h4>
                            <form method="post" action="<?php echo base_url('user/update/') . $query[0]->id; ?>">
                                <input type="hidden" name="created_at" value="<?= $query[0]->created_at; ?>" />
                                <input type="hidden" name="password" value="<?= $query[0]->password; ?>" />
                                <div class="form-group label-floating">
                                    <label class="control-label">Nama Lengkap</label>
                                    <input class="form-control" type="text" name="fullname" required="true" value="<?= $query[0]->fullname; ?>" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Username</label>
                                    <input class="form-control" type="text" name="username" required="true" value="<?= $query[0]->username; ?>" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Email</label>
                                    <input class="form-control" type="text" name="email" required="true" value="<?= $query[0]->email; ?>" />
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Unit Kerja</label>
                                    <select class="selectpicker" name="id_unit" data-style="select-with-transition" title="Unit Kerja" data-size="7" required="true">
                                        <option disabled> Pilih Unit Kerja</option>
                                        <?php
                                        foreach ($units as $unit) {
                                        ?>
                                            <option value="<?= $unit->id ?>" <?php if ($query[0]->id_unit == $unit->id) {
                                                                                    echo 'selected';
                                                                                } ?>><?= $unit->name ?> </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Role</label>
                                    <select class="selectpicker" name="role" data-style="select-with-transition" title="Karyawan / Admin" data-size="3">
                                        <option disabled> Pilih Role</option>
                                        <option value="employee" <?php if ($query[0]->role == 'employee') {
                                                                        echo 'selected';
                                                                    } ?>>Karyawan</option>
                                        <option value="admin" <?php if ($query[0]->role == 'admin') {
                                                                    echo 'selected';
                                                                } ?>>Admin</option>
                                    </select>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Status</label>
                                    <select class="selectpicker" name="is_active" data-style="select-with-transition" title="Aktif / Tidak Aktif" data-size="3">
                                        <option disabled> Pilih Status</option>
                                        <option value="1" <?php if ($query[0]->is_active == '1') {
                                                                echo 'selected';
                                                            } ?>>Aktif</option>
                                        <option value="0" <?php if ($query[0]->is_active == '0') {
                                                                echo 'selected';
                                                            } ?>>Tidak Aktif</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-fill btn-rose">Sunting</button>
                                <a href="<?= $previous; ?>">
                                    <button type="submit" class="btn btn-secondary">Cancel</button>
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>