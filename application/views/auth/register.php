<div class="wrapper wrapper-full-page">
    <div class="full-page register-page" filter-color="black" data-image="<?= base_url() ?>assets/img/register.jpg">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="card card-signup">
                        <h2 class="card-title text-center">Register</h2>
                        <div class="row">
                            <div class="col-md-offset-1 col-md-10">
                                <div class="social text-center">
                                    <h4 class="font-weight-bold text-warning"> <?= $this->session->flashdata('flash'); ?> </h4>
                                </div>
                                <form class="form" method="post" action="<?php echo base_url() ?>user/add">
                                    <div class="card-content">
                                        <input type="hidden" name="role" value="employee">
                                        <input type="hidden" name="is_active" value="0">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">face</i>
                                            </span>
                                            <input type="text" name="fullname" class="form-control" placeholder="Full Name..." required="true">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>
                                            <input type="email" name="email" class="form-control" placeholder="Email..." required="true">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">fingerprint</i>
                                            </span>
                                            <input type="text" name="username" class="form-control" placeholder="Username..." required="true">
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                                            <input type="password" name="password" placeholder="Password..." class="form-control" required="true" />
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                                            <input type="password" name="repassword" placeholder="Konfirmasi Password..." class="form-control" required="true" />
                                        </div>
                                    </div>
                                    <div class="footer text-center">
                                        <button type="submit" class="btn btn-primary btn-round">Registrasi sekarang</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </body>