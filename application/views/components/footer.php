﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">

        </nav>
        <p class="copyright pull-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>
            <a href="http://www.linkedin.com/in/#">Roby Fuadi Zulva</a>, dibuat dengan hati untuk secangkir kopi
        </p>
    </div>
</footer>
</div>
</div>
</body>