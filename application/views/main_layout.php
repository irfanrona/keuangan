<?php
defined('BASEPATH') or exit('No direct script access allowed');


$this->load->view('components/htmlheader');
$this->load->view('components/header');

//content
echo $content;

$this->load->view('components/footer');
$this->load->view('components/htmlfooter');

echo @$jscript;
