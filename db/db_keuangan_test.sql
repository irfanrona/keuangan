/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100413
 Source Host           : localhost:3306
 Source Schema         : db_keuangan_test

 Target Server Type    : MySQL
 Target Server Version : 100413
 File Encoding         : 65001

 Date: 22/03/2022 14:32:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_unit` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `anggaran` int(11) NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (12, 26, 'Kas Perusahaan', 1100000, 1, '2021-08-24 17:56:20', '2022-02-14 12:27:30');
INSERT INTO `categories` VALUES (13, 26, 'Konsumsi', 6200000, 1, '2021-08-24 00:00:00', '2022-03-22 08:27:56');
INSERT INTO `categories` VALUES (14, 26, 'Rumah Tangga', 900000, 0, '2021-08-24 17:56:38', '2022-03-22 14:26:52');
INSERT INTO `categories` VALUES (15, 26, 'Transportasi', 300000, 1, '2021-08-24 18:09:28', '2022-02-14 12:28:36');
INSERT INTO `categories` VALUES (16, 26, 'Sewa Alat Kantor', 3000000, 1, '2021-08-24 22:33:11', '2022-01-27 13:46:50');
INSERT INTO `categories` VALUES (17, 24, 'Pertemuan Klien', 5270000, 1, '2021-08-27 00:00:00', '2022-03-22 08:27:36');
INSERT INTO `categories` VALUES (19, 26, 'BPJS', 600000, 1, '2021-08-27 20:50:58', '2022-01-27 13:47:22');
INSERT INTO `categories` VALUES (24, 22, 'Development testing', 5000000, 1, '2021-12-31 07:51:09', '2022-01-27 13:47:31');
INSERT INTO `categories` VALUES (25, 22, 'Penetration Test', 20000000, 1, '2021-12-31 07:55:42', '2021-12-31 07:55:42');
INSERT INTO `categories` VALUES (26, 23, 'Facebook Ads', 1000000, 1, '2022-01-27 13:47:58', '2022-01-27 13:47:58');
INSERT INTO `categories` VALUES (27, 22, 'Irfan Rona', 1, 0, '2022-02-02 00:00:00', '2022-03-22 14:19:35');
INSERT INTO `categories` VALUES (28, 23, 'Promosi', 50000000, 1, '2022-03-22 00:00:00', '2022-03-22 08:18:32');
INSERT INTO `categories` VALUES (29, 29, 'Service Kendaraan', 8000000, 1, '2022-03-22 00:00:00', '2022-03-22 08:18:53');
INSERT INTO `categories` VALUES (30, 30, 'Vaksin Booster', 16000000, 1, '2022-03-22 00:00:00', '2022-03-22 08:19:14');

-- ----------------------------
-- Table structure for transactions
-- ----------------------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `desc` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` enum('income','expense') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `amount` int(11) NULL DEFAULT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `image` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `approved` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `key_category_id`(`category_id`) USING BTREE,
  CONSTRAINT `key_category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transactions
-- ----------------------------
INSERT INTO `transactions` VALUES (4, 12, 'Rapat dengan klien', 'income', 500000, 'Jumlah klien pastikan tepat', 'card-1.jpg', 1, '2021-08-24 19:53:10', '2022-03-22 08:25:48');
INSERT INTO `transactions` VALUES (6, 15, 'Biaya Transportasi pengiriman barang', 'expense', 250000, NULL, '', 1, '2021-08-24 19:53:10', '2022-01-14 08:58:54');
INSERT INTO `transactions` VALUES (7, 12, 'Iuran kas bulan Juli', 'income', 1000000, 'Iuran bulan Maret disetujui', 'card-2.jpg', 1, '2021-08-24 21:02:48', '2022-03-22 14:06:48');
INSERT INTO `transactions` VALUES (8, 13, 'Donasi anonim untuk makan', 'income', 5300000, NULL, '', 0, '2021-08-24 21:13:21', '2022-03-22 08:24:28');
INSERT INTO `transactions` VALUES (17, 19, 'Bayar BPJS bulan maret', 'expense', 250000, NULL, 'card-3.jpg', 1, '2021-08-24 22:30:28', '2022-03-19 13:54:52');
INSERT INTO `transactions` VALUES (18, 12, 'Beli Buku Baru', 'expense', 100000, NULL, '', 0, '2021-08-24 23:06:25', '2022-03-22 08:25:35');
INSERT INTO `transactions` VALUES (19, 17, 'Pesangon project berhasil', 'income', 281000, NULL, '', 1, '2021-08-27 20:52:33', '2022-01-14 10:31:59');
INSERT INTO `transactions` VALUES (20, 16, 'Bonus cashback pembelian barang ATK', 'income', 900000, NULL, '', 0, '2021-08-27 20:53:28', '2022-03-22 08:15:00');
INSERT INTO `transactions` VALUES (21, 17, 'Ganti rugi biaya meal setiap karyawan', 'expense', 830000, NULL, '', 0, '2021-08-27 20:54:05', '2022-03-22 08:26:32');
INSERT INTO `transactions` VALUES (24, 25, 'Pesangon project berhasil', 'expense', 500000, NULL, '', 0, '2022-01-27 14:13:34', '2022-01-27 14:13:34');
INSERT INTO `transactions` VALUES (26, 26, 'Pesangon project berhasil', 'income', 500000, NULL, '', 0, '2022-03-19 05:05:17', '2022-03-22 08:24:23');
INSERT INTO `transactions` VALUES (53, 25, 'Pesangon project berhasil v2', 'income', 3453450, NULL, '', 0, '2022-03-19 13:10:12', '2022-03-22 08:24:16');
INSERT INTO `transactions` VALUES (56, 30, 'Vaksin ke-3 untuk seluruh anggota PT. Aksara', 'income', 10000000, 'disetujui', '', 1, '2022-03-22 08:20:25', '2022-03-22 14:21:31');
INSERT INTO `transactions` VALUES (57, 30, 'biaya real vaksin untuk PT. Aksara', 'expense', 11540000, 'biaya pengeluaran lebih besar, tidak di setujui', '', 0, '2022-03-22 08:21:02', '2022-03-22 14:22:08');
INSERT INTO `transactions` VALUES (58, 29, 'Service kendaraan pembina', 'expense', 6000000, NULL, '', 0, '2022-03-22 08:22:40', '2022-03-22 08:22:40');
INSERT INTO `transactions` VALUES (59, 15, 'Biaya Transportasi pengiriman barang', 'income', 280000, NULL, '', 0, '2022-03-22 08:28:24', '2022-03-22 08:28:24');
INSERT INTO `transactions` VALUES (60, 19, 'Bayar BPJS bulan maret', 'income', 600000, NULL, '', 0, '2022-03-22 08:29:00', '2022-03-22 08:29:00');
INSERT INTO `transactions` VALUES (61, 29, 'Biaya perawatan mobil dinas', 'income', 1200000, NULL, '', 0, '2022-03-22 08:31:03', '2022-03-22 08:31:16');

-- ----------------------------
-- Table structure for user_units
-- ----------------------------
DROP TABLE IF EXISTS `user_units`;
CREATE TABLE `user_units`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_units
-- ----------------------------
INSERT INTO `user_units` VALUES (22, 'IT Developer', 1, '2021-12-12 03:22:30', '2021-12-12 03:22:30');
INSERT INTO `user_units` VALUES (23, 'Marketing', 1, '2021-12-12 03:25:24', '2021-12-12 03:25:24');
INSERT INTO `user_units` VALUES (24, 'Bussiness Center', 1, '2021-12-12 03:44:17', '2021-12-12 10:12:42');
INSERT INTO `user_units` VALUES (25, 'Oprasional', 0, '2021-12-12 04:00:16', '2021-12-12 10:00:33');
INSERT INTO `user_units` VALUES (26, 'HRD', 1, '2022-01-27 13:45:00', '2022-01-27 13:45:00');
INSERT INTO `user_units` VALUES (27, 'Keuangan', 1, '2022-03-22 08:04:42', '2022-03-22 08:04:42');
INSERT INTO `user_units` VALUES (28, 'Oprasional', 1, '2022-03-22 08:04:47', '2022-03-22 08:04:47');
INSERT INTO `user_units` VALUES (29, 'Logistik', 1, '2022-03-22 08:04:53', '2022-03-22 08:04:53');
INSERT INTO `user_units` VALUES (30, 'Kesehatan', 1, '2022-03-22 08:04:59', '2022-03-22 08:04:59');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_unit` int(11) NULL DEFAULT NULL,
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `remember_token` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT 0,
  `role` enum('employee','admin') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `username`, `email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'robi', 'robi@gmail.com', 'robi1', 22, 'Robi Zulva', NULL, '0000-00-00 00:00:00', '2022-03-22 08:01:34', 1, 'admin');
INSERT INTO `users` VALUES (2, 'irfanrona', 'irfanrona@gmail.com', 'irfanrona', 22, 'Irfan Rona', NULL, '2021-08-27 19:28:27', '2022-03-22 08:01:47', 1, 'employee');
INSERT INTO `users` VALUES (3, 'yana', 'yana@mail.com', 'jamet', 26, 'Yana Suryana', NULL, '2021-08-27 20:05:17', '2022-03-22 08:02:10', 0, 'admin');
INSERT INTO `users` VALUES (4, 'nash', 'nashrul@gunawan.com', '123456', 23, 'Nasrulloh', NULL, '2021-12-12 03:21:36', '2021-12-12 03:27:13', 0, 'employee');
INSERT INTO `users` VALUES (5, 'sae', 'saepul@admin.com', '123456', 23, 'Saepul', NULL, '2021-12-12 03:41:35', '2021-12-12 03:41:53', 1, 'employee');
INSERT INTO `users` VALUES (7, 'nur', 'nur@admin.com', '123456', 25, 'Nurhayati', NULL, '2021-12-12 03:44:07', '2021-12-12 04:00:25', 1, 'employee');
INSERT INTO `users` VALUES (8, 'staffhr', 'staffhr@keuangan.com', 'staffhr', 26, 'Rona', NULL, '2022-01-27 13:42:37', '2022-01-27 14:07:07', 1, 'employee');
INSERT INTO `users` VALUES (9, 'kartap', 'karyawan@dummy.com', 'kartap1', 23, 'Karyawan Dummy', NULL, '2022-03-19 04:54:57', '2022-03-19 04:54:57', 1, 'employee');

SET FOREIGN_KEY_CHECKS = 1;
