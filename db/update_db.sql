ALTER TABLE `db_keuangan_test`.`categories` 
ADD COLUMN `active` int(11) NOT NULL DEFAULT 1 AFTER `name`;

ALTER TABLE `db_keuangan_test`.`users` 
ADD COLUMN `id_unit` int(11) NULL AFTER `password`;

-- ----------------------------
-- Table structure for user_units
-- ----------------------------
DROP TABLE IF EXISTS `user_units`;
CREATE TABLE `user_units`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;